from django.urls import path

from .views import ResultsView, SubmissionView, CatsView, VoteView

app_name = "vote"

urlpatterns = [
    path("vote", VoteView.as_view(), name="vote"),
    path("results", ResultsView.as_view(), name="results"),
    path("submit", SubmissionView.as_view(), name="submission"),
    path("cats", CatsView.as_view(), name="cats"),
]
