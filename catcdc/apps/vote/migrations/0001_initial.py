# Generated by Django 3.0.3 on 2020-03-29 18:34

import conf.storage_backends
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name="Submission",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("cat_name", models.CharField(max_length=255)),
                ("presentation", models.TextField(max_length=500)),
                (
                    "image_1",
                    models.ImageField(
                        storage=conf.storage_backends.PublicMediaStorage(), upload_to=""
                    ),
                ),
                (
                    "image_2",
                    models.ImageField(
                        storage=conf.storage_backends.PublicMediaStorage(), upload_to=""
                    ),
                ),
                (
                    "image_3",
                    models.ImageField(
                        storage=conf.storage_backends.PublicMediaStorage(), upload_to=""
                    ),
                ),
                (
                    "image_4",
                    models.ImageField(
                        storage=conf.storage_backends.PublicMediaStorage(), upload_to=""
                    ),
                ),
                (
                    "image_5",
                    models.ImageField(
                        storage=conf.storage_backends.PublicMediaStorage(), upload_to=""
                    ),
                ),
                (
                    "submitter",
                    models.OneToOneField(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="submitter",
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Vote",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("weight", models.IntegerField()),
                (
                    "submission",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="submission",
                        to="vote.Submission",
                    ),
                ),
                (
                    "voter",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="voter",
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
        ),
    ]
