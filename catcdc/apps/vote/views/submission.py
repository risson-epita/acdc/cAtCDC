from django.conf import settings
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.shortcuts import render, redirect
from django.views.generic import TemplateView

from common.util import is_submission_started, is_submission_ended

from ..models import Submission
from ..forms import SubmissionForm


class SubmissionView(PermissionRequiredMixin, TemplateView):
    permission_required = ("vote.add_submission", "vote.change_submission")
    template_name = "vote/submission.html"

    # pylint:disable=unused-argument
    def get(self, request, *args, **kwargs):
        context = {
            "is_submission_started": is_submission_started(),
            "is_submission_ended": is_submission_ended(),
            "submission_start": settings.SUBMISSION_START.isoformat(),
            "submission_end": settings.SUBMISSION_END.isoformat(),
        }
        current_submission = Submission.objects.filter(submitter=request.user)
        if current_submission.exists():
            form = SubmissionForm(instance=current_submission.first())
        else:
            form = SubmissionForm()
        context["form"] = form
        return render(request, self.template_name, context)

    # pylint:disable=unused-argument,line-too-long
    def post(self, request, *args, **kwargs):
        current_submission = Submission.objects.filter(submitter=request.user)
        if current_submission.exists():
            form = SubmissionForm(
                request.POST, request.FILES, instance=current_submission.first()
            )
        else:
            form = SubmissionForm(request.POST, request.FILES)

        if form.is_valid() and is_submission_started() and not is_submission_ended():
            submission = form.save(commit=False)
            submission.submitter = request.user
            submission.save()
            form.save_m2m()
            return redirect("vote:results")

        context = {
            "is_submission_started": is_submission_started(),
            "is_submission_ended": is_submission_ended(),
            "submission_start": settings.SUBMISSION_START.isoformat(),
            "submission_end": settings.SUBMISSION_END.isoformat(),
        }
        context["form"] = form
        return render(request, self.template_name, context)
