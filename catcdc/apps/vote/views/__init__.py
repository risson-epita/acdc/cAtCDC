from .cats import CatsView  # noqa: F401
from .results import ResultsView  # noqa: F401
from .submission import SubmissionView  # noqa: F401
from .vote import VoteView  # noqa: F401
