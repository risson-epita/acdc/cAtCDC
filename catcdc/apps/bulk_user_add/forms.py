from django import forms
from django.contrib.auth.models import Group


class BulkUserAddForm(forms.Form):
    # pylint:disable=line-too-long
    logins = forms.CharField(
        help_text="Make sure to enter one valid login per line.",
        widget=forms.Textarea(attrs={"rows": "5", "style": "resize: vertical;"}),
    )

    group = forms.ModelChoiceField(queryset=Group.objects.all(), required=True)
