from django.urls import path

from .views import BulkUserAddView

urlpatterns = [
    path("add", BulkUserAddView.as_view()),
]
