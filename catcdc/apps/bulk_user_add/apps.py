from django.apps import AppConfig


class BulkUserAddConfig(AppConfig):
    name = "bulk_user_add"
