from django.conf import settings
from django.shortcuts import render
from django.views.generic import TemplateView

from common.util import (
    is_submission_started,
    is_submission_ended,
    is_vote_started,
    is_vote_ended,
)


class IndexView(TemplateView):
    template_name = "landing_page/index.html"

    def get(self, request, *args, **kwargs):
        context = {
            "is_submission_started": is_submission_started(),
            "is_submission_ended": is_submission_ended(),
            "is_vote_started": is_vote_started(),
            "is_vote_ended": is_vote_ended(),
            "vote_start": settings.VOTE_START.isoformat(),
            "submission_start": settings.SUBMISSION_START.isoformat(),
        }
        return render(request, self.template_name, context)


class ContributorsView(TemplateView):
    template_name = "landing_page/contributors.html"

    def get(self, request, *args, **kwargs):
        context = {}
        return render(request, self.template_name, context)
