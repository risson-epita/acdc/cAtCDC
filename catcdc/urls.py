"""scoreboard URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.shortcuts import render

urlpatterns = [
    path("admin/", admin.site.urls),
    path("accounts/", include("social_django.urls", namespace="social")),
    path("", include("landing_page.urls")),
    path("vote/", include("vote.urls", namespace="vote")),
    path("bulk_user_add/", include("bulk_user_add.urls")),
]


# pylint:disable=unused-argument
def page_not_found(request, exception):
    return render(request, "404.html")


# pylint:disable=invalid-name
handler404 = "urls.page_not_found"
